<?php

namespace Weezo\StatusType\Core;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{

    /**
     * Reescreve o método para os métodos de relations poderem retornar vazios
     * @param string $method
     * @return Collection
     * @throws LogicException
     */
    protected function getRelationshipFromMethod($method)
    {
        $relations = $this->$method();

        if ( $relations instanceof Collection){
            // "fake" relationship
            return $this->relations[$method] = $relations;
        }
        if (! $relations instanceof Relation) {
            throw new LogicException('Relationship method must return an object of type '
                .'Illuminate\Database\Eloquent\Relations\Relation');
        }

        return $this->relations[$method] = $relations->getResults();
    }

}
<?php

namespace Weezo\StatusType\Providers;

use Illuminate\Support\ServiceProvider;

class StatusTypeServiceProvider extends ServiceProvider
{

    /**
     * The console commands.
     *
     * @var bool
     */
    protected $commands = [
        'Weezo\StatusType\Commands\AddStatusType',
    ];

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {

        //Registra os comandos para o artisan
        $this->commands($this->commands);
    }

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishDatabase();
    }

    public function provides()
    {
        return ['status-type'];
    }

    /**
     * Publica as arquivos de database na app
     * @link https://laravel.com/docs/5.2/packages#public-assets
     */
    public function publishDatabase()
    {
        $this->publishes([__DIR__ . '/../../Resources/database/migrations/' => database_path('migrations')],'migrations');
    }



}
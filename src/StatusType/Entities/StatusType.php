<?php

namespace Weezo\StatusType\Entities;

use Illuminate\Database\Eloquent\Collection;
use Weezo\AccessControl\Entities\UserStatus;
use Weezo\StatusType\Core\BaseModel;

class StatusType extends BaseModel
{
    protected $table = 'status_types';
    protected $fillable = ['title','slug'];

    public $timestamps = false;

    /*
    |--------------------------------------------------------
    | Relationship Methods
    |--------------------------------------------------------
    */
    public function userStatus(){
        if(class_exists('UserStatus')){ return $this->hasMany(UserStatus::class); }
        return new Collection();
    }

}
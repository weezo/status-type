<?php

namespace Weezo\StatusType\Commands;

use Illuminate\Console\Command;
use Weezo\StatusType\Entities\StatusType;

class AddStatusType extends Command
{

    protected $signature = 'status-type:add';

    protected $description = "Adiciona um tipo de status no banco de dados";


    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {


        do{
            $title = trim($this->ask('Informe o título ao tipo de status. Ex.: Erro'));
            $slug = trim($this->ask('Informe a slug. Não use espaços e nem acentos. Ex.: danger'));

            $reg = new StatusType;
            $reg->create([
                'title' => $title,
                'slug' => $slug
            ]);

            $this->info('Tipo de status cadastrado com sucesso!');

        }while($this->confirm('Gostaria de cadastrar outro?'));

    }


}
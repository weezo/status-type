<?php


namespace Weezo\StatusType\Tests\Entities;

use Weezo\StatusType\Entities\StatusType;
use Weezo\StatusType\Tests\AbstractTestCase;

class StatusTypeTest extends AbstractTestCase
{

    public function setUp()
    {
        parent::setUp();
        $this->migrate();
    }

    public function test_check_if_a_status_type_can_be_persisted()
    {
        $statusType = StatusType::create([
            'title' => 'Inativo',
            'slug'  => 'gray'
        ]);
        $this->assertEquals('Inativo',$statusType->title);

        $statusType = StatusType::all()->first();
        $this->assertEquals('Inativo',$statusType->title);

    }

}
# Weezo PKG StatusType

Pacote para Laravel 5 desenvolvido por **Weezo Soluções em Informática**

> **EM DESENVOLVIMENTO**

> CRUD de StatusType. Utilizado como requerimento de outros pacotes.

## Índice

* [Instalação](#instalação)
    * [Dependências](#1-dependência)
    * [Provider](#2-provider)
    * [Publicando arquivos](#3-publicando-arquivos)
    * [Migrations](#4-migrations)
* [Idiomas](#idiomas)
* [Desenvolvedores](#desenvolvedores)

## Instalação

### 1. Dependência
Adicione o repositório e o require no `composer.json`:
```json
{
    "repositories":[
        {
            "type": "vcs",
            "url": "https://github.com/weezo/pkg-status-type"
        }
    ],
    "require": {
        "weezo/status-type": "dev-development"
    }
}
```

### 2. Provider
Adicione o provider no arquivo `config/app.php`:
```php
'providers' => [
    Weezo\StatusType\Providers\StatusTypeServiceProvider::class,
]
```

### 3. Publicando arquivos
Publique os arquivos do pacote executando o comando:
```shell
php artisan vendor:publish
```
> Será publicado: Migrations

### 4. Migrations
Execute as migrations, com o comando:
```shell
php artisan migrate
```

## Idiomas
* Português (Brasil)

## Desenvolvedores
* [Juliana Macêdo](https://github.com/jullymac)